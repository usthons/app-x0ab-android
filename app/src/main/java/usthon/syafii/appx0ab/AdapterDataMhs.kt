package usthon.syafii.appx0ab

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_mahasiswa.*
import kotlinx.android.synthetic.main.activity_main.*

class AdapterDataMhs(
    val dataMhs: List<HashMap<String, String>>,
    val mahasiswaActivity: MahasiswaActivity) :
    RecyclerView.Adapter<AdapterDataMhs.HolderDataMhs>(){
    override fun onCreateViewHolder( p0: ViewGroup, p1: Int): AdapterDataMhs.HolderDataMhs {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_mahasiswa,p0,false)
        return  HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataMhs.size
    }

    override fun onBindViewHolder(p0: AdapterDataMhs.HolderDataMhs, p1: Int) {
        val data = dataMhs.get(p1)
        p0.txtNim.setText(data.get("nim"))
        p0.txtNama.setText(data.get("nama"))
        p0.txtProdi.setText(data.get("nama_prodi"))

        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener({
            val pos = mahasiswaActivity.daftarProdi.indexOf(data.get("nama_prodi"))
            mahasiswaActivity.spProdi.setSelection(pos)
            mahasiswaActivity.edNim.setText(data.get("nim"))
            mahasiswaActivity.edNama.setText(data.get("nama"))
            Picasso.get().load(data.get("url")).into(mahasiswaActivity.imgUpload)

        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)

    }

    class HolderDataMhs(v: View) : RecyclerView.ViewHolder(v){
        val txtNim = v.findViewById<TextView>(R.id.txtNim)
        val txtNama = v.findViewById<TextView>(R.id.txtNama)
        val txtProdi = v.findViewById<TextView>(R.id.txtProdi)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}