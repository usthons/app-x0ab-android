package usthon.syafii.appx0ab

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_prodi.*

class AdapterProdi(val dataProdi : List<HashMap<String,String>>, val prodiActivity: ProdiActivity) :
    RecyclerView.Adapter<AdapterProdi.HolderDataProdi>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterProdi.HolderDataProdi {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_prodi, p0, false)
        return HolderDataProdi(v)
    }

    override fun getItemCount(): Int {
        return dataProdi.size
    }

    override fun onBindViewHolder(holder: AdapterProdi.HolderDataProdi, position: Int) {
        val data = dataProdi.get(position)
        holder.txtNamaProdi.setText(data.get("nama_prodi"))
        if(position.rem(2) == 0) holder.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else holder.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        holder.cLayout.setOnClickListener({
            prodiActivity.idProdi = data.get("id_prodi").toString()
            prodiActivity.edNamaProdi.setText(data.get("nama_prodi"))
        })
    }

    class HolderDataProdi(v: View) : RecyclerView.ViewHolder(v) {
        val txtNamaProdi = v.findViewById<TextView>(R.id.txNamaProdi2)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }

}