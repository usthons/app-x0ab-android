package usthon.syafii.appx0ab

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_mahasiswa.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MahasiswaActivity:AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper : MediaHelper
    lateinit var mhsAdapter : AdapterDataMhs
    lateinit var prodiAdapter : ArrayAdapter<String>
    var daftarMhs = mutableListOf<HashMap<String,String>>()
    var daftarProdi = mutableListOf<String>()
    val mainUrl = "http://192.168.43.62/kampus/"
    val url = mainUrl+"show_data.php"
    var url2 = mainUrl+"data_prodi.php"
    var url3 = mainUrl+"up_in_del_query.php"
    var imStr = ""
    var pilihProdi = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mahasiswa)
        mhsAdapter = AdapterDataMhs(daftarMhs,this)
        mediaHelper = MediaHelper(this)
        listMhs.layoutManager = LinearLayoutManager(this)
        listMhs.adapter = mhsAdapter

        prodiAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarProdi)
        spProdi.adapter = prodiAdapter
        spProdi.onItemSelectedListener = itemSelected

        imgUpload.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUpload -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btnInsert -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btnDelete -> {
                queryInsertUpdateDelete("delete")
            }
            R.id.btnUpdate -> {
                queryInsertUpdateDelete("update")
            }
            R.id.btnFind -> {
                showDataMhs(edNama.text.toString().trim())
            }
        }
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spProdi.setSelection(0)
            pilihProdi = daftarProdi.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihProdi = daftarProdi.get(position)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data,imgUpload)
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataMhs("")
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("nim",edNim.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_prodi",pilihProdi)
                    }
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("nim",edNim.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_prodi",pilihProdi)
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("nim",edNim.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaProdi(namaProdi : String){
        val request = object :StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarProdi.add(jsonObject.getString("nama_prodi"))
                }
                prodiAdapter.notifyDataSetChanged()
    },
    Response.ErrorListener { error ->
        Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
    }){
        override fun getParams(): MutableMap<String, String>{
            val hm = HashMap<String, String>()
            hm.put("nama_prodi",namaProdi)
            return hm
        }
    }
    val queue = Volley.newRequestQueue(this)
    queue.add(request)
    }


    fun showDataMhs(namaMhs : String){
        val request = object :StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMhs.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("nim",jsonObject.getString("nim"))
                    mhs.put("nama",jsonObject.getString("nama"))
                    mhs.put("nama_prodi",jsonObject.getString("nama_prodi"))
                    mhs.put("url",jsonObject.getString("url"))
                    daftarMhs.add(mhs)
                }
                mhsAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama",namaMhs)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataMhs("")
        getNamaProdi("")
    }
}